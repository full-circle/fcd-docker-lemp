# README #

A lightweight LEMP (Linux, Nginx, Mysql, PHP) server configuration using Docker Compose.

### What It's For ###

Quick local development tasks of php applications. Minimal configuration needed to get started.

### Prerequisites ###

* Install and configure Docker. (https://docs.docker.com/install/)
* Install Compose. (if not installed alongside Docker.) (https://docs.docker.com/compose/install/#install-compose)

### How do I get up and running? ###

Clone this repo.
`git clone https://full-circle@bitbucket.org/full-circle/fcd-docker-lemp.git my-project`

Add application or site files to `/site`.

Run `docker-compose up --build -d`

Check Localhost:8080 and Voila.

### Future Features ###

* Support for Maria alongside MySQL.
